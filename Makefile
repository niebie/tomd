# TODO > Use GNU automake tools

all:tomd tomc

tomd:./src/tomd/main.c ./src/common/guile_helpers.c
	gcc -static -o tomd \
	-I/usr/local/include/guile/2.2 \
	./src/tomd/main.c ./src/common/guile_helpers.c

tomc:./src/tomc/main.c ./src/common/socketio.c
	gcc -o tomc ./src/tomc/main.c ./src/common/socketio.c -static
