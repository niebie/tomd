;; Copyright (C) 2018 Thomas Balzer

;; This file is part of tomd.

;; tomd is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; tomd is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with tomd.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tomd job)
  #:use-module (srfi srfi-9)
  #:export (create-job make-job
            job-command-line c-job-cmd
            job-args c-job-args
            job-start-trigger c-job-start-trigger
            job-end-trigger c-job-end-trigger
            job-name c-job-name
            c-check-job))

;;; records
(define-record-type <job>
  (make-job name command-line args start-trigger end-trigger)
  job?
  (name job-name)
  (command-line job-command-line)
  (args job-args)
  (start-trigger job-start-trigger)
  (end-trigger job-end-trigger))

;;; this sillyness is because i'm not sure how to expand macros in scm_call
(define (c-check-job obj)
  (job? obj))

(define (c-job-cmd obj)
  (job-command-line obj))

(define (c-job-args obj)
  (job-args obj))

(define (c-job-start-trigger obj)
  (job-start-trigger obj))

(define (c-job-end-trigger obj)
  (job-end-trigger obj))

(define (c-job-name obj)
  (job-name obj))

;;; functions
(define (get-keyword-value args keyword default)
  (let ((keyword-value (memq keyword args)))
    (if (and keyword-value (>= (length keyword-value) 2))
        (cadr keyword-value)
        default)))

(define (create-job . rest)
  (let ((command-line  (get-keyword-value rest #:command-line  #f))
        (args          (get-keyword-value rest #:args          (list)))
        (start-trigger (get-keyword-value rest #:start-trigger 'login))
        (end-trigger   (get-keyword-value rest #:end-trigger   #f))
        (name          (get-keyword-value rest #:name          #f)))
    ;; do thing with keyword-ed variables
    ;; (display "settings:") (newline)
    ;; (format (current-output-port)
    ;;         "command-line:~a" command-line)
    ;; (newline)
    ;; (format (current-output-port)
    ;;         "args:~a" args)
    ;; (newline)
    ;; (format (current-output-port)
    ;;         "start-trigger:~a" start-trigger)
    ;; (newline)
    ;; (format (current-output-port)
    ;;         "end-trigger:~a" end-trigger)
    ;; (newline)

    ;; create a new object that represents the args given.
    (make-job name
              command-line
              args
              start-trigger
              end-trigger)
    ))
