/* Copyright (C) 2018 Thomas Balzer */

/* This file is part of tomd. */

/* tomd is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* tomd is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with tomd.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <stdio.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <unistd.h>

#include "../../include/macros.h"
#include "../../include/manifest.h"

static int sfd;

static void header(void)
{
  tomc_p("Tom's Client, Copyright (C) 2018 Thomas Balzer");
  tomc_p("GPL v3 or later license.");
}

static void init(void)
{
  /* init socket connection */
  sfd =
    socket(PF_LOCAL,
           SOCK_STREAM,
           0);
  if(sfd < 0){
    perror("socket");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_un addr;
  addr.sun_family = AF_LOCAL;
  sprintf(addr.sun_path, "/run/user/1000/tomd/socket");

  if(connect(sfd, (struct sockaddr *) &addr, SUN_LEN(&addr)) != 0){
    perror("connect");
    exit(EXIT_FAILURE);
  }
  setup_socket(sfd);
}

static char *default_name = "no_name";
static struct{
  char status, stop, kill, start;
  char  *name;
} options;

static void extract_options(int argc, char **argv)
{
  /* -status <name>  Get status - if we thought we ran it, current real status */
  /* -stop <name>    Run provided command to gently stop */
  /* -kill <name>    Aggressive killing of process (signal 15) */
  /* -start <name>   Run name */
  if(argc == 1){
    /* only tomc given */
    tomc_p("no args given.");
    exit(EXIT_SUCCESS);
  }

  options.name = NULL;

  for(int i = 1;
      i < argc;
      i++){
    char *arg = argv[i];
#define X(op) {if(strcmp("-" #op, arg) == 0){options.op = 1; continue;}}
    X(status);
    X(stop);
    X(kill);
    X(start);
#undef X
    /* assume last non option was the name */
    options.name = arg;
  }

  if(options.name == NULL){
    options.name = default_name;
  }
}

static void print_options(void)
{
  tomc_p("---------");
  tomc_p("  name: %s", options.name);
  tomc_p("  kill: %d", options.kill);
  tomc_p("status: %d", options.status);
  tomc_p("  stop: %d", options.stop);
  tomc_p(" start: %d", options.start);
  tomc_p("---------");
}

static char write_buf[100];
static char socket_buf[100];

#define SOCK_WRITE(X) {                                             \
    strcpy(write_buf, X);                                           \
    int write_len = strlen(write_buf);                              \
    ssize_t wrote =                                                 \
      write(sfd, write_buf, write_len);                             \
    /* tomc_p("wrote '%s' (%d)", write_buf, write_len);                 */ \
    if(wrote != write_len){                                         \
      perror("[tomc] write size mismatch");                         \
      exit(EXIT_FAILURE);                                           \
    }}

int last_read_size = 0;

static void do_read(char *line)
{
  /* printf("got %s as input.\n", line); */
  strcpy(socket_buf, line);
  /* printf("socket_buf: %s\n", socket_buf); */
}

#define SOCK_READ {                             \
    socket_read(do_read);                       \
  }
  /*   int size = read(sfd, socket_buf, 100);      \ */
  /*   if(size == 0) {                             \ */
  /*     tomc_p("didn't actually get anything.");  \ */
  /*     socket_buf[0] = '\0';                     \ */
  /*   }else{                                      \ */
  /*     tomc_p("__debug__:size=%d", size);        \ */
  /*   }                                           \ */
  /*   socket_buf[size] = '\0';                    \ */
  /*   tomc_p("__debug__: '%s'", socket_buf);\ */
  /*   last_read_size = size;\ */
  /* } */
#define SOCK_READ_X(X){                                                 \
    SOCK_READ;                                                          \
    if(strcmp(socket_buf, X) != 0){                                     \
      tomc_p("protocol error. instead of ACK we got '%s'", socket_buf); \
      exit(EXIT_FAILURE);                                               \
    }                                                                   \
  }
#define SOCK_ACK {SOCK_READ_X("ACK")};
#define SOCK_REQUEST(X) {                       \
    SOCK_WRITE(X);                              \
    SOCK_ACK;                                   \
    SOCK_WRITE(options.name);                   \
    SOCK_ACK;                                   \
  }

static void write_client(void)
{
  SOCK_WRITE("client:tomc");
  SOCK_ACK;
}

static void write_requests(void)
{
  write_client();

  /* go through options to figure out what we want to do */
  if(options.kill){
    SOCK_REQUEST("kill");

    if(options.start == 1 ||
       options.stop  == 1){
      tomc_p("can only kill and status at once.");
    }
  } else {
    if(options.start){
      if(options.stop){
        tomc_p("can't start and stop at once.");
      }
      SOCK_REQUEST("start");
    }
    if(options.stop){
      SOCK_REQUEST("stop");
    }
  }

  if(options.status){
    SOCK_REQUEST("status");
  }

  while(1){
    /* printf("looking for that last ack.\n"); */
    SOCK_READ;
    if(strcmp(socket_buf, "ACK") == 0){
      /* tomc_p("got the ending ack."); */
      write(sfd, "ACK", sizeof "ACK");
      break;
    }else{
      /* printf("in else.\n"); */
      /* int len = strlen(socket_buf); */
      /* int total = len; */
      /* if(last_read_size == 0){ */
      /*   continue; */
      /* } */
      tomc_p(socket_buf);
      /* while(total != last_read_size -1){ */
      /*   tomc_p("__debug__:total=%d, size=%d", total, last_read_size); */
      /*   char *ptr = socket_buf + total; */
      /*   tomc_p("'%s'", ptr); */
      /*   total += strlen(ptr); */
      /* } */
    }
  }
}

int main(int argc, char **argv)
{
  header();
  extract_options(argc, argv);
  print_options();
  init();
  write_requests();

  /* tomc_p("dying of our own accord."); */

  return EXIT_SUCCESS;
}
