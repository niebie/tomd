/* Copyright (C) 2018 Thomas Balzer */

/* This file is part of tomd. */

/* tomd is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* tomd is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with tomd.  If not, see <http://www.gnu.org/licenses/>. */

#include <libguile.h>
#include <pwd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/wait.h>

#include "../../include/job.h"
#include "../../include/macros.h"

#ifdef GUILE_CAPABLE
#include "../../include/scm_interface.h"
#endif

#define MANIFEST_LOC "/.config/tomd/init/manifest.scm"

#define MAX_JOBS 10

static int root_job = -1;

#ifdef GUILE_CAPABLE
static struct job jobs[MAX_JOBS] =
  {
   {
    "bash",      /* name */
    "/bin/bash", /* cmd */
    {},          /* args  */
    0,           /* pid */
    0,           /* last_status */
    0,           /* redirect */
    1,           /* root */
   },
   /* rest null */
   {}
  };
#else
static struct job jobs[MAX_JOBS] = {};
#endif

#ifdef GUILE_CAPABLE
static struct job load_job(SCM job_list, int index)
{
  struct job ret = { NULL };
  SCM scm_cur_job = SCM_ARR(job_list, index);

  if(scm_is_false(job_predicate(scm_cur_job))){
    tomd_p("job %d wasn't a real job type.", index);
    return ret;
  }

  SCM scm_name = get_name(scm_cur_job);
  SCM scm_cmd = get_cmd(scm_cur_job);
  SCM scm_args = get_args(scm_cur_job);

  /* TODO > Handle these. */
  /* planned is to be able to run at different points, and at */
  /* the request of a user. for testing we do things all at */
  /* boot up. */
  SCM scm_start_trigger = get_start_trigger(scm_cur_job);
  SCM scm_end_trigger = get_end_trigger(scm_cur_job);

  char *job_name = scm_to_locale_string(scm_name);
  char *job_cmd = scm_to_locale_string(scm_cmd);
  char *real_args[10];
  int jlen = scm_to_int(scm_length(scm_args));
  for(int j = 0;
      j < jlen;
      j++){
    real_args[j] =
      scm_to_locale_string(SCM_ARR(scm_args, j));
  }

  ret.name = job_name;
  ret.cmd = job_cmd;

  for(int j = 1;
      j <= jlen;
      j++){
    ret.args[j] = real_args[j - 1];
  }
  ret.args[jlen + 1] = NULL;
  ret.args[0] = ret.cmd;

  return ret;
}

static void *load_manifest(void *args)
{
  char *manifest_loc;
  if(!args){
    tomd_p("arg to load_manifest is NULL");
    exit(EXIT_FAILURE);
  }
  manifest_loc = (char *)args;

  scm_c_primitive_load(args);

  SCM scm_job_list =
    scm_c_public_ref("tomd manifest", "job-list");

  if(scm_is_false(scm_job_list)){
    tomd_p("no job-list found in manifest.scm");
    return NULL;
  }

  if(scm_is_false(scm_list_p(scm_job_list))){
    tomd_p("job-list found, but isn't a list.");
    return NULL;
  }

  int i;
  int len = SCM_LIST_LEN(scm_job_list);
  tomd_p("len=%d, max=%d", len, MAX_JOBS);
  for(i = 0;
      i < len && i < MAX_JOBS;
      i++){
    jobs[i] = load_job(scm_job_list, i);
  }
  tomd_p("looked at %d jobs.", i);
  jobs[i].cmd = NULL;
}
#endif

static char *lookup_user_folder(void)
{
#ifdef GUILE_CAPABLE
  uid_t user = getuid();
  struct passwd *userpasswd =
    getpwuid(user);

  /* take the manifest location and sub ~ for pw_dir */
  char *buffer = (char *)malloc(sizeof(char) * 300);
  strcpy(buffer, userpasswd->pw_dir);
  int len = strlen(userpasswd->pw_dir);
  strcpy(buffer + len, MANIFEST_LOC);
  return buffer;
#else
  return NULL;
#endif
}

void load_jobs(void)
{
  char *manifest = lookup_user_folder();
#ifdef GUILE_CAPABLE
  tomd_p("Loading jobs from '%s'", manifest);

  void *res =
    scm_with_guile(load_manifest,
                   (void *) manifest);
#endif
  tomd_p("Stubbed job loading.");

  tomd_p("Finished loading.");

  int i = 0;
  while(1){
    if(i >= MAX_JOBS ||
       jobs[i].cmd == NULL) {
      break;
    }
    tomd_p("JOB <%d>:", i);
    tomd_p("     cmd:'%s'", jobs[i].cmd);
    int j = 1;
    while(1){
      if(i >= 10 ||
         jobs[i].args[j] == NULL){
        break;
      }
      tomd_p(" arg [%d]:'%s'", j, jobs[i].args[j]);
      j++;
    }
    i++;
  }
}


static int stdincache;
static int stdoutcache;
static int stderrcache;

void silent(void)
{
  /* cache all filedes */
  stdincache = dup(STDIN_FILENO);
  stdoutcache = dup(STDOUT_FILENO);
  stderrcache = dup(STDERR_FILENO);

  tomd_p("going silent.");

  /* close default filedes */
  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  tomd_p("now silent.");
}

#define LOG_DIR "/var/log/tomd/"

void run_job(int index)
{
  struct job *job = &jobs[index];

  pid_t pid = fork();
  if(pid == 0){                 /* child */
    if(job->redirect){
      /* redirect to a file */
      char buf[100];
      strcpy(buf, LOG_DIR);
      strcpy(buf + strlen(LOG_DIR), job->name);
      tomd_p("redirecting stdout to %s.", buf);
      if(unlink(buf) != 0){
        tomd_p("file couldn't be removed.");
      }

      int fd = open(buf, O_WRONLY | O_CREAT, 0644);
      if(fd < 0){
        tomd_p("couldn't open file.");
        return;
      }

      dup2(fd, STDOUT_FILENO);
      dup2(fd, STDERR_FILENO);
      close(fd);
    }
    execvp(job->cmd, job->args);
    tomd_panic("execvp for '%s' failed", job->cmd);
  }else{                        /* parent */
    tomd_p("forked [%d] to run '%s'", pid, job->cmd);
    job->pid = pid;
    waitpid(job->pid, &job->last_status, WNOHANG);
  }
}

struct job *lookup_job(char *my_job_name)
{
  for(int i = 0;
      i < MAX_JOBS;
      i++){
    if(jobs[i].name == NULL){
      continue;
    }
    if(strcmp(jobs[i].name, my_job_name) == 0){
      return &jobs[i];
    }
  }
  return NULL;
}

void run_jobs(void)
{
  int i;
  for(i = 0;
      i < MAX_JOBS;
      i++){
    if(jobs[i].cmd == NULL){
      tomd_p("out of jobs");
      return;
    }else{
      tomd_p("running job [%d] '%s'", i, jobs[i].cmd);
      run_job(i);
      if(jobs[i].root){
        if(root_job != i && root_job != -1){
          tomd_p("error! only one job can be root.");
        }else{
          root_job = i;
        }
      }
    }
  }
}

static int delay = 0;

void check_root_job(void)
{
  delay++;
  if(delay % 100 != 0){
    return;
  }else{
    delay = 0;
  }
  if(root_job != -1){
    int n_pid = waitpid(jobs[root_job].pid, &jobs[root_job].last_status, WNOHANG);
    if(n_pid != jobs[root_job].pid){
      return;
    }

    if(WIFEXITED(jobs[root_job].last_status)){
      /* we died, restart it */
      tomd_p("restarting root job. (last pid = %d, last status = %d)",
             jobs[root_job].pid, jobs[root_job].last_status);
      run_job(root_job);
    }
  }
}
