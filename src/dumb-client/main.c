/* Copyright (C) 2018 Thomas Balzer */

/* This file is part of tomd. */

/* tomd is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* tomd is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with tomd.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <stdio.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <unistd.h>

static int sfd;

static void init(void)
{
  /* init socket connection */
  sfd =
    socket(PF_LOCAL,
           SOCK_STREAM,
           0);
  if(sfd < 0){
    perror("socket");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_un addr;
  addr.sun_family = AF_LOCAL;
  sprintf(addr.sun_path, "/run/user/1000/tomd/socket");
  
  if(connect(sfd, (struct sockaddr *) &addr, SUN_LEN(&addr)) != 0){
    perror("connect");
    exit(EXIT_FAILURE);
  }
}

#define HALLO "hallo there from dumb-client"

void write_hallo(void)
{
  printf("writing hallo\n");
  ssize_t wrote =
    write(sfd, HALLO, sizeof HALLO);
  printf("wrote %d bytes.\n", wrote);
}

int main(int argc, char **argv)
{
  printf("dumb client startup.\n");

  init();
  write_hallo();
  
  return EXIT_SUCCESS;
}
