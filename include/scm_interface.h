/* Copyright (C) 2018 Thomas Balzer */

/* This file is part of tomd. */

/* tomd is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* tomd is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with tomd.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef _SCM_INTERFACE_
#define _SCM_INTERFACE_
#include "macros.h"

WRAP_SCM_FUNCTION_1("tomd job", "c-check-job", job_predicate)
WRAP_SCM_FUNCTION_1("tomd job", "c-job-name", get_name)
WRAP_SCM_FUNCTION_1("tomd job", "c-job-cmd", get_cmd)
WRAP_SCM_FUNCTION_1("tomd job", "c-job-args", get_args)
WRAP_SCM_FUNCTION_1("tomd job", "c-job-start-trigger", get_start_trigger)
WRAP_SCM_FUNCTION_1("tomd job", "c-job-end-trigger", get_end_trigger)

#endif
